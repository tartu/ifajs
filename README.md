ifajs
=====

A module of tartujs. This module, as its name implies, is *interaction for action*

Some interesting links:
*	http://shamansir.github.io/blog/articles/generating-functional-parsers/
*	https://www.learnosity.com/blog/2014/10/your-own-parser-possible-with-peg-js/
*	http://www.codeproject.com/Articles/29713/Parsing-Expression-Grammar-Support-for-C-Part
*	http://stackoverflow.com/questions/3612836/simple-parsing-questions-using-peg-js
*	http://sergimansilla.com/blog/writing-a-javascript-interpreter-for-dbn-using-canvas-I/
*	http://nathansuniversity.com/
*	https://coderwall.com/p/316gba/beginning-parsers-with-peg-js
*	https://github.com/PhilippeSigaud/Pegged/wiki/PEG-Basics

