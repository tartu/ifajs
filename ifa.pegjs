{
    function makeInteger(o) {
        return parseInt(o.join(""), 10);
    }
}

start
    =   additive / text

/* write a string */
text
    =   numbers
    /   characters
numbers
    =   numbers: [0-9]+ 
    {
        return numbers.join("")
    }
characters
    =   text: [a-z]+ 
    {
        return text.join("")
    }
/* end of write a string */

/* arithmatic grammar */
additive
    =   left:multiplicative "+" right:additive 
    { 
        return left + right; 
    }
    / multiplicative
multiplicative
    =   left:primary "*" right:multiplicative
    {
        return left * right; 
    }
    / primary
primary
    =   integer
    / "(" additive:additive ")"
    {
        return additive;
    }
integer "integer"
    =   digits:[0-9]+ 
    { 
        return makeInteger(digits);
    }
/* end of arithmatic grammar */